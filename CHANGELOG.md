# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.3 - 2024-09-30(05:52:46 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v0.5.2 - 2024-04-30(13:21:00 +0000)

### Other

- Move component to prpl-foundation gitlab

## Release v0.5.1 - 2023-09-26(08:05:20 +0000)

### Fixes

- [USP] deregistered_path can be repeated

## Release v0.5.0 - 2023-09-25(12:55:19 +0000)

### New

- [USP] Use latest protobuf schema

## Release v0.4.0 - 2023-07-20(13:08:12 +0000)

### New

- [USP] Add UDS connect record to library

## Release v0.3.0 - 2023-04-26(07:18:30 +0000)

### New

- [USP] Add NotifType AmxNotification for ambiorix events

## Release v0.2.0 - 2022-09-05(13:08:50 +0000)

### New

- [USP] Upstep version of libuspprotobuf

## Release v0.1.3 - 2022-04-27(11:33:18 +0000)

### Other

- Enable auto opensourcing

## Release v0.1.2 - 2022-02-21(08:10:51 +0000)

### Fixes

- Paths field in deregister must be plural

### Other

- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v0.1.1 - 2022-01-14(08:48:25 +0000)

### Changes

- Replace supports_add boolean with shared boolean

## Release v0.1.0 - 2021-12-06(09:46:26 +0000)

### New

- [USP] Implement a discovery service

## Release v0.0.10 - 2021-12-06(08:07:43 +0000)

### Changes

- [USP] Make protobuf compiler version check less strict

## Release v0.0.9 - 2021-08-25(07:43:16 +0000)

### Other

- Issue: amx/usp/libraries/libprotobuf#1 Component should have a changelog with notable changes
- [libprotobuf] Protobuf compilers needed on host instead of target

## Release v0.0.8 - 2021-06-05(10:02:51 +0200)

### Changes

- [USP] libprotobuf can currently not be installed in openwrt

## Release v0.0.7 - 2021-06-05(10:02:51 +0200)

### Changes

- Enabled cross compilations
